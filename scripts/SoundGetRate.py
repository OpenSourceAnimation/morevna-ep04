#===========  MODIFY PARAMETERS HERE =================

oldpath="//sound/track-1-cosh-man.wav"
newpath="//render/sound/voice-raw-1-cosh-man.flac.wav"

#=====================================================

import bpy
import wave
import os
import shutil
import subprocess
import sys

target_rate = "48000"

def inspect(i):
    if i.type == 'SOUND':
        path = bpy.path.abspath(i.sound.filepath)
        
        #print("---- "+path)
        valid_rate=False
        rate=""
        commandline = ["soxi", path]
        proc = subprocess.Popen(commandline, stdout=subprocess.PIPE)
        for line in proc.stdout.readlines():
            line = line.decode(sys.stdout.encoding)
            if not line:
                break
            #print(line)
            sys.stdout.flush()

            if line.find("Sample Rate    : 48000")!=-1:
                valid_rate = True
                #print("!!!!!")
            if line.find("Sample Rate")!=-1:
                rate = line.strip()
                #print("22222")
            #print(line)

        
        #try:
        #    with wave.open(path, "rb") as wave_file:
        #        rate = wave_file.getframerate()
        #except:
        #    rate = "ERROR"
        if (valid_rate==False):
            print("%s - %s" % (rate,path))
            if not i.sound.filepath.endswith("wav"):
                old=i.sound.filepath
                i.sound.filepath="//render"+old[1:]+".wav"
            if i.sound.filepath.endswith("wav") and i.sound.filepath.startswith("//render"):
                commandline=["renderchan", path]
                #subprocess.check_call(commandline)
            if i.sound.filepath.endswith("wav") and (not i.sound.filepath.startswith("//render")):
                if not os.path.exists(path+".orig"):
                    shutil.copy(path, path+".orig")
                commandline=["sox", path+".orig", "-e", "signed-integer" , "-b" "24", path, "rate", "-v", target_rate]
                print("zzz")
                subprocess.check_call(commandline)
                
    if i.type=='META':
        for j in i.sequences:
            inspect(j)


seq=bpy.context.scene.sequence_editor.sequences_all

for i in seq:
    inspect(i)
