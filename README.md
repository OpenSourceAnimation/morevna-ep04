Source files of **Morevna Episode 4**.

You can fetch contents of this directory using RSync:

```
rsync -avz --exclude render rsync://archive.morevnaproject.org/sources/morevna-ep04/ ~/morevna-ep04/
```

Alternatively, you can [clone from GitLab](https://gitlab.com/OpenSourceAnimation/morevna-ep04).

Note: before cloning from GitLab, please make sure that you have [Git LFS extension](https://git-lfs.github.com/) installed.
